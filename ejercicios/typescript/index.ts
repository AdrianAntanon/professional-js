/* console.log('Hello, TypeScript');

function add(a: number, b: number){
    return a + b;
}

const sum = add(2, 3); */

/* ------ TIPOS DE DATOS ------ */
// Booleanos
let muted: boolean = true;

// Números
let age = 6
let numerador: number = 42;
let denominador: number = age;
let resultado = numerador/denominador;

// String
let nombre: string = 'Adri';
let saludo = `Hola, me llamo ${nombre}`;

// Arreglos o Arrays
let people: string[] = [];
people=['Adri','Andrea'];

let peopleAndNumbers: Array<string | number> = [];
peopleAndNumbers.push(people[0]);
peopleAndNumbers.push(10);

// Enum
/* enum Color{
    Rojo = "Rojo",
    Verde = "Verde",
    Azul = "Azul",
}

let colorFavorito: Color = Color.Rojo;

console.log(`Mi color favorito es ${colorFavorito}`); */

// Any
let comodin: any = "Joker";
comodin = {type: "Wildcard"};

// Object
let someObject: object = {type: "Wildcard"};


/* ------ FUNCIONES ------ */

function add(a:number, b:number): number{
    return a + b;
}

const sum = add(3,10);

function createAdder(a:number): (number) => number{
    return function(b:number){
        return b + a;
    }
}

const addFor = createAdder(4);
const fourPlus6 = addFor(6);

console.log(fourPlus6);

function fullName(firstName:string, lastName?:string):string{
    return `${firstName} ${lastName}`;
}
// Si no le pasamos segundo parámetro nos daría error, poniendo ?: le decimos que o bien es string o undefined, en caso de no querer dejarlo vacío podemos hacer un lastName:string = 'XXX' y así hacemos que por defecto tenga ese valor a no ser que reciba un string.
const Adri = fullName('Adri');


/* ------ INTERFACES ------ */

enum Colores{
    Rojo = "rojo",
    Verder = "verde",
}

interface Rectangulo{
    ancho: number,
    alto: number,
    color: Colores,
}

let rect: Rectangulo = {
    ancho: 6,
    alto: 10,
    color: Colores.Rojo,
}

function area(r: Rectangulo): number{
    return r.alto * r.ancho;
}

const areaRect = area(rect);
console.log(areaRect);

rect.toString = function(){
    return this.color ? `Un rectángulo ${this.color}` : `Un rectángulo incoloro`;
}

console.log(rect.toString());